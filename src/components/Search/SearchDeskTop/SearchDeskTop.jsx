import React, { useEffect, useRef, useState } from 'react'
import { useMediaQuery } from 'react-responsive';
import { useNavigate } from 'react-router';
import styled, { keyframes } from 'styled-components'
import InputSearchCheckIn from '../InputSearchCheckIn';
import InputSearchCheckOut from '../InputSearchCheckOut';
import InputSearchGuest from '../InputSearchGuest';
import InputSearchPlace from '../InputSearchPlace';
let startHeight=document.getElementById('user')?.offsetHeight;
const startSearch1 = keyframes`
from {
  transform :scale(0);
   margin-top:0;
   opacity:1;
   height:0px;
 }
 to {
   opacity:1;
   transform :scale(1);
  height:${90}px;
   width:100% ;
 }
`;
const hiddenTriggerOption = keyframes`
from {
  transform :scale(0);
   opacity:1;
 }
 to {
   left:0;
   bottom:0;
   opacity:0;
   transform :scale(1);
   width:100% ;
 }
`;
const showTriggerOption =keyframes`
from {
 
   opacity:0;
   transform :scale(1);
   width:100% ;
  
 }
 to {
  transform :scale(1);
   opacity:1;
 }
`;
const startSearch2 = keyframes`
from {
  width:90%;
opacity:0;
bottom:50%;
transform :scale(0);
left:50%;
transform: translateX(-50%);

}
to {
  width:90%;
opacity:1;
bottom:10px;
left:50%;
 transform :scale(1);
 transform: translateX(-50%);
 height:${90/1.5}px;
 };
}
`
const SearchStyled = styled.div`
// width:70%;
.startSearch1{
animation:${startSearch1} 0.2s ease-in-out forwards;
   .hidden_trigger{
      width:100%;
      animation:${hiddenTriggerOption} 0.3s ease-in-out forwards;
   }
   #service{
     top:0;
   }
}
// .show_trigger{
//   animation:${showTriggerOption} 3s ease-in-out forwards;
//  }
.startSearch2{
  animation:${startSearch2} 0.3s ease-in-out forwards;
 }

`; 
let ClickOutSide=(handel,isSearch)=>{
  let searchRef=useRef();
                    useEffect(()=>{
                        let maybeHandler=(e)=>{
                            if(!searchRef.current.contains(e.target)){
                                handel();
                            }
                        };
                      if (isSearch){
                        document.addEventListener("mousedown",maybeHandler)
                      }
                      else{
                        document.removeEventListener("mousedown",maybeHandler)
                      }
                     return ()=>{
                        document.removeEventListener("mousedown",maybeHandler)
                     }
                    },[isSearch])
  return searchRef;
}
function SearchDeskTop() {
  const [isSearch,setIsSearch]=useState(false);
  let searchRef=ClickOutSide(()=>{
    setIsSearch(false);
  },isSearch)
  return (
   <SearchStyled ref={searchRef}>
     <div   className={`${isSearch?"startSearch1":""} relative`}>
       <div id='service' className='absolute left-1/2 -top-20 -translate-x-1/2 space-x-3 text-center  w-max mx-auto  p-2 ' data-header-menu>
                  <span className=' text-sm' data-active>Nơi ở</span>
                  <span className=' text-sm'>Trải nghiệm</span>
                  <span className=' text-sm'>Trải nghiệm trực tuyến</span>
            </div>
     <div id='trigger' className={`${isSearch?'hidden_trigger':'show_trigger'} absolute -translate-x-1/2 left-1/2  w-max mx-auto border-2 space-x-3 flex justify-between rounded-full bg-white  p-2 shadow hover:shadow-xl`}>
     <button className='hover:bg-slate-200 border-black rounded-3xl '  data-index={1} onClick={(e)=>{setIsSearch(e.target.dataset.index)}}>địa điểm</button>
     <button className='hover:bg-slate-200 border-black rounded-3xl'  data-index={2} onClick={(e)=>{setIsSearch(e.target.dataset.index)}}>tuần bất kì</button>
     <button className='hover:bg-slate-200 border-black rounded-3xl'  data-index={3} onClick={(e)=>{setIsSearch(e.target.dataset.index)}}>thêm khách</button>
     </div>
     </div>
      <div className={` ${isSearch?"startSearch2":"hidden"} absolute w-1/6 bg-white shadow-2xl container rounded-full -translate-x-1/2 bottom-0  `}>
              <div className='p-2 m-0 flex [&>*]:border-2 [&>*]:rounded-full [&>*]:width-full justify-between items-center h-full'>
                  
                  <div className='w-2/5 tablet:w-3/5 h-full '>
                  {isSearch&&<InputSearchPlace autoFocus={isSearch==1?true:false}></InputSearchPlace>}
                  </div>
                  <div className='h-full w-1/5 flex justify-between items-center p-2'>
                     <span className=' w-full capitalize tablet:text-xs text-center text-sm'>ngày đến:</span>
                  <InputSearchCheckIn autoFocus={isSearch==2?true:false}></InputSearchCheckIn>
                  </div>
                  <div className='h-full w-1/5 flex justify-between items-center p-2'>
                  <span className=' w-full capitalize tablet:text-xs text-center text-sm'>ngày đi:</span>
                  <InputSearchCheckOut ></InputSearchCheckOut>
                  </div>
                  <div className='w-1/5  h-full'>
                  <InputSearchGuest autoFocus={isSearch==3?true:false}></InputSearchGuest>
                  </div>
              </div>
      </div>
   </SearchStyled>
  )
}

export default SearchDeskTop