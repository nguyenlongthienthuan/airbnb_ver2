import React, { useEffect, useMemo, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setPlace } from '../../redux/SearchSlice';
import { placesService } from '../../services/API_Airbnb.service';

function InputSearchPlace({autoFocus}) {
  let dispatch=useDispatch();
  let [isStartSearch,setIsStartSearch]=useState(false)
  
  let place=useSelector((state)=>{
    return state.SearchSlice.place;
  })
  let [listPlaces,setListPlaces]=useState([]);
  let [searchPlace,setSearchPlace]=useState('');
  let inputPlaceRef=useRef();
  let keySearchRef=useRef();
  useEffect(()=>{
     if(autoFocus){
        inputPlaceRef.current.getElementsByTagName("input")[0].focus();
     }
       keySearchRef.current.value=place.viTri;
      let maybeHandel=(e)=>{
          if(!inputPlaceRef.current.contains(e.target))
          { 
            setIsStartSearch(false);
          }
      } 
      if (isStartSearch){
        document.addEventListener("mousedown",maybeHandel)
      }
      else{
        document.removeEventListener("mousedown",maybeHandel)
      }
    //  fetch list place 
          placesService.getListPlaces().then((res)=>{setListPlaces(res.data.content.map((item)=>{
            let {quocGia,tenViTri,tinhThanh,id}=item;
            return {maViTri:id,viTri: `${quocGia+','+tenViTri+','+tinhThanh}`};
          }))});
     return ()=>{
      document.removeEventListener('mousedown',maybeHandel)
     }
  },[isStartSearch,place,autoFocus])
  // let listSearchPlaces=places.filter((item)=>{ return item.viTri.toLowerCase().includes(place.viTri?place.viTri.toLowerCase():"")});

  let listPlaceSearch=useMemo(()=>{
    return listPlaces.filter((item)=>{return item.viTri.toLowerCase().includes(searchPlace?.toLowerCase())})
  },[searchPlace])
  return (
    <div ref={inputPlaceRef} className='h-full w-full relative '>
      <input ref={keySearchRef} placeholder='chọn điểm đến' onFocus={()=>{setIsStartSearch(true)}} className=' text-center focus:border-stone-400 h-full w-full rounded-3xl' type="text"   onChange={(e)=>{keySearchRef.current.value=e.target.value;setSearchPlace(e.target.value)}}  />
       {isStartSearch?
    <div className={`${!listPlaceSearch.length?"h-max":"h-28"} absolute top-110% left-0 bg-white shadow-xl w-full text-center  rounded-2xl overflow-hidden hover:overflow-y-scroll `}>
       {listPlaceSearch.map((item,index)=>{
        return <p key={index} onClick={()=>{dispatch(setPlace(item))}} className='hover:bg-slate-100'>{item.viTri}</p>
      })}
    </div>:<></>}
   </div>
  )
}

export default InputSearchPlace