import moment from 'moment';
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router';
import styled, { keyframes } from 'styled-components';
import DatePicker from "react-datepicker";
import { placesService } from '../../../services/API_Airbnb.service';
import { setCheckIn, setPlace } from '../../../redux/SearchSlice';
import InputSearchCheckIn from '../InputSearchCheckIn';
import InputSearchCheckOut from '../InputSearchCheckOut';
import InputSearchGuest from '../InputSearchGuest';

const animation=keyframes`
from{
 top:100%;
}to{
  top:0%;
}
`
const PopupSearchMobileStyled=styled.div`
  // #search.active{
  //     #editSearch{
  //       display:block;
  //     }
  //     #resultSearch{
  //       display:none;
  //     }
  // }
  #form{
    animation:${animation} 0.2s ease-in-out forwards;
  }
  // .listSearch{
     .search{
          &[data-active]{
             border:2px solid black;
            #editSearch{
              display:block;
            }
            #resultSearch{
              display:none;
            } 
          }
          :hover{
            border:1px solid white ;
          }
      
     }
  }
`

function PopupSearchMobile({isStartSearch,setIsStartSearch}) {
  let navigate=useNavigate();
  let inputPlace = useRef();
  let slkhach=useRef();
  let dispatch = useDispatch();
  const [places, setplaces] = useState([]);
  const search = useSelector((state) => {
    return state.SearchSlice;
  })
 
  let { place, checkIn, checkOut, guest } = search;
  
  useEffect(() => {
    placesService.getListPlaces().then(
      (res) => {
        // console.log(res.data.content);
        setplaces((state) => {
          return res.data.content.map((item) => { return { viTri: `${item.tenViTri + "," + item.tinhThanh + "," + item.quocGia}`, maViTri: item.id } })
        })
      }
    ).catch((err) => { console.log(err); })

  }, [isStartSearch])
  
  let activeSearch=useCallback((e)=>{
      let active=e.target.closest(".listSearch").querySelector('[data-active]'); 
      e.target.closest(".search").dataset.active=true;
     if(!e.target.closest(".inputSearch") && active){
      delete active.dataset.active;
     }
  },[isStartSearch])
  let listSearchPlaces = places.filter((item) => { return item.viTri.toLowerCase().includes(place.viTri ? place.viTri.toLowerCase() : "") });
  return (isStartSearch ? <PopupSearchMobileStyled>
    <div id='form'  className='listSearch w-screen h-screen fixed z-50 bg-neutral-200 p-3 '>
      <div className=' relative p-2 text-center space-x-3 '>
        <i onClick={()=>{dispatch(setIsStartSearch(false))}} className="fa fa-times-circle absolute top-1/2 -translate-y-1/2 left-5 text-xl"></i> <b>chỗ ở</b><b>trãi nghiệm</b>
      </div>
      <div id='search' onClick={(e)=>{activeSearch(e)}} className='search bg-white rounded-xl p-3 mt-5 active' data-active>
          <div id='editSearch' className='hidden'>
             <div><h1 className=' font-bold'>Bạn sẽ đi đâu</h1></div>
             <div  className=' border-2 rounded-xl'>
              <label id='' htmlFor="" className='inputSearch p-3 flex items-center space-x-4'>  <i className="fa fa-search"></i><input ref={inputPlace} value={place.viTri} onChange={(e) => { dispatch(setPlace({ viTri: e.target.value })) }} placeholder='tim kiem dia diem' className='w-full' type="text" /></label>
            </div>
            <div id='listPlaces' className=' overflow-y-scroll p-3' style={{ height: `30vh` }}>
              {
                listSearchPlaces.map((item,index) => {
                  return <p key={index} className=' hover:bg-gray-400 cursor-pointer' onClick={(e) => { dispatch(setPlace(item)); dispatch(setIsStartSearch(2)) }}>{item.viTri}</p>
                })
              }
               </div>
               </div>
            <div id='resultSearch' className='flex justify-between'><p>Địa điểm</p> <p>{place.viTri}</p></div>  
      </div>
      <div id='search' onClick={(e)=>{activeSearch(e); 
    }} className='search bg-white rounded-xl p-3 mt-5 active' >
          <div id='editSearch'  className='hidden'>
             <div><h1 className=' font-bold'>Chuyến đi của bạn sẽ diễn ra khi nào?</h1></div>
             <div  className=' border-2 rounded-xl'>
              <label id='' htmlFor="" className='inputSearch p-3 flex items-center space-x-4'>  
              <InputSearchCheckIn></InputSearchCheckIn>
              <InputSearchCheckOut></InputSearchCheckOut>
              </label>
            </div>
            
               </div>
            <div id='resultSearch' className='flex justify-between'><p>Thời gian</p> <p>{checkIn=="" || checkOut==""?<i>chọn thời gian </i>:moment(checkIn).format("DD-MM")+"->"+moment(checkOut).format("DD-MM")}</p></div>  
      </div>
      <div id='search' onClick={(e)=>{activeSearch(e);slkhach.current.focus();}} className='search bg-white rounded-xl p-3 mt-5 active' >
          <div id='editSearch' className='hidden'>
             <div><h1 className=' font-bold'>Nhập số lượng khách trong chuyến đi</h1></div>
             <div  className=' border-2 rounded-xl'>
              <label id='' htmlFor="" className='inputSearch   space-x-4 w-full'>  
                 <InputSearchGuest></InputSearchGuest>
              </label>     
            </div>
               </div>
            <div id='resultSearch' className='flex justify-between'><p>Số lượng khách: </p> <p>{guest==null?<i>nhập sl khách</i>:guest}</p></div>  
      </div>
      <div className='mt-5'>
      <button onClick={()=>{ 
       if(place.maViTri!=""){
        navigate(`/listRoom/${place.maViTri}`);dispatch(setIsStartSearch(false))
       }   else{
        alert("hãy chọn điểm đến")
      }
       }} className='py-1 px-3 bg-red-100 rounded-xl hover:border-2'>Tìm phòng</button>
      </div>
    </div>
  </PopupSearchMobileStyled> : <></>
  )
}

export default PopupSearchMobile