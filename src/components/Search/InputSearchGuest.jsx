import React, { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { setGuest } from '../../redux/SearchSlice';

function InputSearchGuest(autoFocus) {
  let inputRef=useRef();
  let dispatch=useDispatch();
  let place=useSelector((state)=>{
    return state.SearchSlice.place;
  })
  let guest=useSelector((state)=>{
    return state.SearchSlice.guest;
  })
  let {maViTri}=place;
  let navigate=useNavigate();
  useEffect(()=>{
  if (autoFocus){
    inputRef.current.focus();
  }
  },[autoFocus])
  return (
    <div className='flex items-center h-full'>
                  <input ref={inputRef} placeholder='nhập số lượng khách' onChange={(e)=>{dispatch(setGuest(e.target.value))}} value={guest} className=' w-full h-full rounded-full text-center' type="number" />
                     <div onClick={()=>{
                      if(maViTri!=""){navigate(`/listRoom/${maViTri}`)}
                      else{
                        alert("hãy chọn điểm đến")
                      }
                     }} className='h-9 w-9 bg-blue-50 flex flex-shrink items-center justify-center rounded-full'> <i className="fa fa-search flex items-center"></i>
                     </div>
                  </div>
  )
}

export default InputSearchGuest