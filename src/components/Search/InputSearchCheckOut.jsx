import React, { useEffect, useRef } from 'react'
import DatePicker from 'react-datepicker'
import { useDispatch, useSelector } from 'react-redux'
import { setCheckOut } from '../../redux/SearchSlice';

function InputSearchCheckOut() {
  let dispatch=useDispatch();
  const checkOut=useSelector((state)=>{
    return state.SearchSlice.checkOut;
  })
  const checkIn=useSelector((state)=>{
    return state.SearchSlice.checkIn;
  })
  let minDate=new Date(checkIn).setDate(new Date(checkIn).getDate()+1);
  let inputRef=useRef();
  useEffect(()=>{
    inputRef.current.input.type='button';
  },[])
 
  return (
       
    <DatePicker ref={inputRef}  placeholderText='chọn ngày đi'    autoFocus={false}  minDate={minDate}   selected={new Date(checkOut)-(new Date())==0?minDate:checkOut} 
    className='w-full  text-center rounded-3xl h-full tablet:text-xs'  dateFormat='dd-MM-yyyy' onChange={(date)=>{
    dispatch(setCheckOut(date))
    }} />
 
  )
}

export default InputSearchCheckOut