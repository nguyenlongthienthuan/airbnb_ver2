import React, { useEffect, useRef } from 'react'
import DatePicker, { ReactDatePicker } from 'react-datepicker'
import { useDispatch, useSelector } from 'react-redux'
import { setCheckIn } from '../../redux/SearchSlice';

function InputSearchCheckIn({autoFocus}) {
  let dispatch=useDispatch();
  const checkIn=useSelector((state)=>{
    return state.SearchSlice.checkIn
  })
  let inputRef=useRef();
  useEffect(()=>{
    // console.log(inputRef.current);
    inputRef.current.input.type='button';
    inputRef.current.input.backgoround='white';
   if (autoFocus){
      setTimeout(()=>{inputRef.current.input.focus();},300);
   }
  },[autoFocus])
  return (
    <DatePicker ref={inputRef}   placeholderText='chọn ngày đến'   minDate={new Date()} selected={checkIn}  
    className=' text-center rounded-full w-full  h-full tablet:text-xs'   dateFormat='dd-MM-yyyy' onChange={(date)=>{
    dispatch(setCheckIn(date))
    }} />
  )
}

export default InputSearchCheckIn