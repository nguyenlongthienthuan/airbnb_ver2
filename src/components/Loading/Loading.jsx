import React from 'react'
import { useSelector } from 'react-redux'
import { BounceLoader } from 'react-spinners';

function Loading() {
    const isLoading=useSelector((state)=>{
        return state.PopUpHandel.isLoading;
    })
   
  return (
   isLoading? 
    <div className=' h-screen w-screen flex justify-center items-center bg-black'>
       <BounceLoader color="#36d7b7" />
       </div>
       :<></>
  )
}

export default Loading