import React, { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import styled, { keyframes } from 'styled-components';
import { setIsLogin, setIsPopUp } from '../../redux/PopUpHandel';
import { setUserInfor } from '../../redux/UserSlice';
import { localService } from '../../services/local.service';
import { userService } from '../../services/user.service';
let showPopup=keyframes`
   from{
   bottom:-100%;
   }to{
    bottom:50%;
    transform: translateY(50%);
   }
` 
const PopUpLoginStyled=styled.div`
   .active{
    animation:${showPopup} 0.3s ease-in-out forwards;
   }
`

function PopUpLogin() {
    let navigate=useNavigate()
    let passWordRef=useRef();
    let userNameRef=useRef();
    let isLogin = useSelector((state) => {
      return state.PopUpHandel.isLogin;
  })
    let dispatch=useDispatch();
     let popUpRef = useRef();
    useEffect(()=>{
        let maybeHandel=(e)=>{
            if(!popUpRef.current?.contains(e.target) && e.target.id!="login"){ 
                dispatch(setIsLogin(false))
                    
            };
        };
        document.addEventListener("mousedown",maybeHandel)
        if(!isLogin){
            document.removeEventListener("mousedown",maybeHandel)
        }
        return ()=>{
                document.removeEventListener("mousedown",maybeHandel)
            
        }
    },[isLogin])
    let login=()=>{
        const params={
          email: userNameRef.current.value,
          password: passWordRef.current.value,
        }
        userService.postLogin(params).then(
          (res)=>{
            localService.set(res.data.content)
         
           dispatch( setUserInfor(res.data.content))
          
            dispatch(setIsLogin(false))
            window.scrollTo(0, 0);
          }
        ).catch(
          (err)=>{
            alert(err.response.data.content)
          }
        )
    } 
    
  return (
     <PopUpLoginStyled>
        <div className={`${isLogin?'block':"hidden"} fixed top-0 w-screen h-screen bg-black bg-opacity-60 z-50 flex justify-center `}>
     <div ref={popUpRef} className={`${isLogin?'active':"hidden"} rounded-2xl w-1/2 max-h-max mobile:w-full mobile:max-h-max bg-white absolute  flex items-center justify-start flex-col`}
       >
        
       <div  className='w-full relative p-3 text-center'>
        <h1 className=''>Đăng nhập hoặc đăng kí</h1>
        <div onClick={()=>{dispatch(setIsLogin(false))}} className=' hover:cursor-pointer absolute top-0 right-10'><i  class="fa fa-times"></i></div>
       </div>
       <hr className='w-full'/>
       <div className=' space-y-5 p-5 flex flex-col justify-center flex-1'>
       <div>tài khoản: <input className=' border-2 rounded' ref={userNameRef} placeholder='email' type="text" /></div>
       <div>mật khẩu: <input className=' border-2 rounded'  ref={passWordRef} placeholder='mật khẩu' type="password" /></div>
       <div className=' space-x-5'>
      <button  className='py-1 px-3 border-2 rounded-xl' type="submit" onClick={login}>Login</button>
       <a  href="#" onClick={()=>{
        navigate("/signup");
       setIsLogin(false)
       }}>SignUp</a>
      </div>
       </div>
       </div>
   </div>
     </PopUpLoginStyled>
     
  )
}

export default PopUpLogin