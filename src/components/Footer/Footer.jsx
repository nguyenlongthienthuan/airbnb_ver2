import React from 'react'
import styled from 'styled-components'
const FooterStyled=styled.div`
 p{
    color:#aaa;
 }
`
function Footer() {
  return (
   <FooterStyled>
     <div className='  bg-stone-200 p-3 '>
        <div className='grid grid-cols-4 gap-3 container mx-auto mobile:grid-cols-2'>
        <div>
            <h3>GIỚI THIỆU</h3>
            <p>Phương thức hoạt động</p>
            <p>Trang tin tức</p>
            <p>Nhà đầu tư</p>
            <p>Airbnb plus</p>
            <p>Airbnb lux</p>
            <p>HotelTonight</p>
            <p>Airbnb work</p>
            <p>Nhờ có hóst mọi điều đều có thể</p>
            <p>Cơ hội nghề nghiệp</p>
            <p>Thư của nhà sáng lập</p>
        </div>
        <div>
            <h3>CỘNG ĐỒNG</h3>
            <p>Sự đa dạng và cảm giác thân thuộc</p>
            <p>Tiện nghi phù hợp cho người khuyết tật</p>
            <p>Đối tác liên kết airbnb</p>
            <p>Chỗ ở cho tuyến đầu</p>
            <p>lượt giới thiệu của khách</p>
            <p>Airbnb.org</p>
        </div>
        <div>
            <h3>ĐÓN TIẾP KHÁCH</h3>
            <p>Cho thuê nhà</p>
            <p>Tổ chức trãi nghiệm trực tuyến</p>
            <p>Tổ chức trãi nghiêm</p>
            <p>Đón tiếp khách có trách nhiệm</p>
            <p>Trung tâm tài nguyên</p>
            <p>Trung tâm cộng đồng</p>
        </div>
        <div>
            <h3>HỖ TRỢ</h3>
            <p>Biện pháp ứng phó đại dịch covid-19 của chúng tôi</p>
            <p>Trung tâm trợ giúp</p>
            <p>Cách tuỳ chọn huỷ</p>
            <p>Hỗ trợ khu dân cư</p>
            <p>Tin cậy và an toàn</p>
            
        </div>
        </div>
    </div>
   </FooterStyled>
  )
}

export default Footer