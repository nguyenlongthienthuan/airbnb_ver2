import React, { useEffect, useRef, useState } from 'react'
import MenuUser from './MenuUser'

function User() {
    const [isOpen,setIsOpen]=useState(false);
        let handelMenuUser=()=>{
            setIsOpen(()=>{return !isOpen})
        }
        let userRef=useRef()
                    useEffect(()=>{
                        let maybeHandler=(e)=>{
                            if(!userRef.current.contains(e.target)){
                                setIsOpen(false);
                            }
                        };
                      if (isOpen){
                        document.addEventListener("mousedown",maybeHandler)
                      }
                      else{
                        document.removeEventListener("mousedown",maybeHandler)
                      }
                     return ()=>{
                        document.removeEventListener("mousedown",maybeHandler)
                     }
                    },[isOpen])
          
        
  return (
    <div id='user' ref={userRef}  className=' relative shadow-xl  py-2 px-3 rounded-3xl hover:shadow-2xl border-white' style={{border:`1px solid silver`}}>
     <div onClick={handelMenuUser} >
     <i className="fa fa-bars"></i><i className="fa fa-user"></i>
     </div>
     {isOpen?   <MenuUser isOpen={isOpen} handelMenuUser={handelMenuUser}></MenuUser>:<></>}
    </div>
  )
}

export default User