import React from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router'
import styled, { keyframes } from 'styled-components'
import { setIsLogin, setIsPopUp } from '../../redux/PopUpHandel'
import { localService } from '../../services/local.service'

const ShowMenu = keyframes`
from{
    height: 0;
    opacity:0;
  }to{
    opacity:1;
    height:max-content;
  }
`
const MenuStyled = styled.div`
  #menu{
    &.active{
    animation: ${ShowMenu} 0.1s ease-in-out forwards;}
    *{ padding:2px 2px;
        &:hover{
            background:#aaaa;
            border-radius: 16px;
        }
    }
  }
`
function MenuUser({ isOpen ,handelMenuUser}) {
    let navigate = useNavigate();
    let dispatch = useDispatch();
   
    return (
            <MenuStyled>
                {!localService.get() ?
                    <div id='menu' className={`${isOpen?"active":"hidden"} absolute w-max p-2 bg-white shadow-2xl rounded-2xl -bottom-2 z-50 translate-y-full right-0 overflow-hidden`}>
                        <p onClick={() => { navigate("/signup") }}>Đăng kí</p>
                        <p id='login' onClick={() => {dispatch(setIsPopUp(true));dispatch(setIsLogin(true));handelMenuUser() }}>
                            Đăng nhập</p>
                        <hr />
                        <p>Cho thuê chỗ ở qua airbnb</p>
                        <p>Tổ chức trãi nghiệm</p>
                        <p>Trợ giúp</p>
                    </div> :
                    <div id='menu' className=' absolute w-max p-2 bg-white shadow-2xl rounded-2xl -bottom-2 translate-y-full right-0 overflow-hidden'>
                        <p className='  capitalize' onClick={() => { 
                             if(localService.get().user.role.toLocaleLowerCase()=='user')
                               {navigate(`/user/${localService.get().user.id}`)}
                               else{
                                navigate(`/admin`)
                               }
                           }}>{localService.get().user.name}</p>
                        <p onClick={()=>{
                            localService.delete();
                            window.location.reload();
                        }}>đăng xuất</p>
                    </div>
                }
            </MenuStyled>
            
    )
}

export default MenuUser