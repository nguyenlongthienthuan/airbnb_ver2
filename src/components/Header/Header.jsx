import React from 'react'
import { DesktopReponsive, MobileReponsive, TabletReponsive } from '../../HOC/Reponsive'
import Header_Tablet_Desktop from './Header_Tablet_Desktop/Header_Tablet_Desktop'
import MobileHeader from './mobileHeader/MobileHeader'

function Header() {
  return (
    <div className=''>
        <DesktopReponsive>
            <Header_Tablet_Desktop>
            </Header_Tablet_Desktop>
        </DesktopReponsive>
        <TabletReponsive>
            <Header_Tablet_Desktop>
            </Header_Tablet_Desktop>
        </TabletReponsive>
       <MobileReponsive>
        {/* <HeaderMobile></HeaderMobile> */}
        <MobileHeader></MobileHeader>
        {/* <Header_Tablet_Desktop>
            </Header_Tablet_Desktop> */}
       </MobileReponsive>
    </div>
  )
}

export default Header