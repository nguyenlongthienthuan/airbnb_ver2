import React from 'react'
import { useNavigate } from 'react-router'
import SearchDeskTop from '../../Search/SearchDeskTop/SearchDeskTop'
import User from '../../User/User'

function Header_Tablet_Desktop() {
  let navigate=useNavigate()
  return (
  <header className='w-screen px-2 py-1 relative'>
   <div className="[&>*]:hover:cursor-pointer   p-4 mx-auto flex tablet:w-screen  justify-between items-start  shadow-2xl border-t-2 rounded-full">
   <div onClick={()=>{navigate('/')}} id="logo" className=' flex items-center space-x-1 font-bold'>
        <div className="icon text-3xl"><i className="fab fa-airbnb"></i></div>
        <div className="name text-2xl">AirBnB</div>
    </div>
    <div id="search" className='  w-4/6 tablet:w-3/6   '>
        <SearchDeskTop></SearchDeskTop>
    </div>
  
    <div id="user" className=' flex items-center space-x-2 '>
            <div className=' flex items-center space-x-2'>
            <p >Đón tiếp Khách</p>
            <i className="fa fa-globe"></i>
            </div>
            <User></User>
    </div>
   </div>
  </header>
  )
}

export default Header_Tablet_Desktop