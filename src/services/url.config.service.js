import axios from "axios";
import { store } from "..";
import { setIsLoading } from "../redux/PopUpHandel";
import { localService } from "./local.service";



let baseURL="https://airbnbnew.cybersoft.edu.vn";
let tokenCyber="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAwMSIsIkhldEhhblN0cmluZyI6IjMwLzA5LzIwMzEiLCJIZXRIYW5UaW1lIjoiMTk0ODQ5MjgwMDAwMCIsIm5iZiI6MTYwMTIyNjAwMCwiZXhwIjoxOTQ4NjQwNDAwfQ.4l-eTzlgVnFczfvc2Or7BNPOcaesY3Kwc8RoNm-o-6M";

let configHeader=()=>{
    return {
        TokenCybersoft: tokenCyber,
        token:localService.get()?.token,
    }
}
export const https=axios.create({
    baseURL,
    headers:configHeader(),
})

https.interceptors.request.use(function (config) {
    // store.dispatch(setIsLoading(true));
    // Do something before request is sent
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

// Add a response interceptor
https.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    // store.dispatch(setIsLoading(false));
    return response;
  }, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  });