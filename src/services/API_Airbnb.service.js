import { https } from "./url.config.service";
export const placesService={
    getListPlaces:()=>{
        let uri="/api/vi-tri";
        return https.get(uri);
    },
    getPlace:(id)=>{
        let uri=`/api/vi-tri/${id}`;
        return https.get(uri);
    },
    getPhanTrangTimKiem:(params)=>{
        let uri="/api/vi-tri/phan-trang-tim-kiem";
        return https.get(uri,params);
    },
    getPhongTheoViTri:(params)=>{
        let uri="/api/phong-thue/lay-phong-theo-vi-tri";
        return https.get(uri,{params})
    },
    getPhongThue:(id)=>{
       
        let uri=`/api/phong-thue/${id}`
        return https.get(uri);
    },
    getDatPhong:()=>{
        let uri='/api/dat-phong';
        return https.get(uri);
    },
    postDatPhong:(params)=>{
        let uri='/api/dat-phong';
        return https.post(uri,params)
    },
    getCommentRoom:(id)=>{
        let uri=`/api/binh-luan/lay-binh-luan-theo-phong/${id}`;
        return https.get(uri)
    },
    postCommentRoom:(params)=>{
        let uri=`/api/binh-luan`;
        return https.post(uri,params)
    },
    getPhongPhanTrangTimKiem:(params)=>{
        let uri='/api/phong-thue/phan-trang-tim-kiem';
        return https.get(uri,{params})
    }
}