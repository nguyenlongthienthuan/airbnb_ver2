import { https } from "./url.config.service";


export const userService={
    postLogin:(params)=>{
        let uri="/api/auth/signin";
      return  https.post(uri,params)
    },
    postSignUp:(params)=>{
       let uri="/api/auth/signup";
       return https.post(uri,params)
    },
    getInforUser:(id)=>{
      let uri=`/api/users/${id}`;
      return https.get(uri)
    },
    getDatPhongUser:(MaNguoiDung)=>{
      let uri=`/api/dat-phong/lay-theo-nguoi-dung/${MaNguoiDung}`;
      return https.get(uri)
    },
    putUser:(id,params)=>{
      let uri=`/api/users/${id}`;
      return https.put(uri,params);
    }
    
}