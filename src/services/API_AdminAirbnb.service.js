import { https } from "./url.config.service";

export const API_ADMIN_SERVICE={
    getListUser:(params)=>{
        let uri='/api/users/phan-trang-tim-kiem';
        return https.get(uri,{params});
    },
    deleteUser:(id)=>{
        // console.log(id.id);
        let uri=`/api/users?id=${id.id}`;
        return https.delete(uri);
    },
    putUserId:(id,params)=>{
        let uri=`/api/users/${id}`;
        return https.put(uri,params);
    },
    putViTri:(id,params)=>{
        let uri=`/api/vi-tri/${id}`;
        return https.put(uri,params);
    },
    deleteViTri:(id)=>{
        let uri=`/api/vi-tri/${id}`;
        return https.delete(uri);
    },
    postViTri:(params)=>{
        let uri=`/api/vi-tri`;
        return https.post(uri,params);
    },
    deletePhong:(id)=>{
        let uri=`/api/phong-thue/${id}`;
        return https.delete(uri);
    },
    putPhong:(id,params)=>{
        let uri=`/api/phong-thue/${id}`;
        return https.put(uri,params)
    },
    putDatPhong:(id,params)=>{
        let uri=`/api/dat-phong/${id}`;
        return https.put(uri,params)
    },
    deleteDatPhong:(id)=>{
        let uri=`/api/dat-phong/${id}`;
        return https.delete(uri);
    }
}