import React, { useEffect, useState } from 'react'
import { useMediaQuery } from 'react-responsive';
import { useParams } from 'react-router'
import SercueView from '../../HOC/SercueView';
import { localService } from '../../services/local.service';
import { userService } from '../../services/user.service'
import ItemPhongDat from './ItemPhongDat';
import PopUpEditInfor from './PopUpEditInfor';

function UserPage() {
  
  let {id}=useParams();
  let [isOpenPopUpEdit,setisOpenPopUpEdit]=useState(false);
  let [userInfor,setUserInfo]=useState({});
  let [listBooked,setListBooked]=useState([]);
  let {avatar,birthday,email,gender,name,password,phone,role}=userInfor
  useEffect(()=>{
    userService.getInforUser(parseInt(id)).then(
      (res)=>{
        setUserInfo(res.data.content);
         userService.getDatPhongUser(res.data.content.id).then(
          (res)=>{
            setListBooked(res.data.content);
          }
         )
      }
    )
  },[])
  return (
  <SercueView>
    
     <div className='w-full' style={{
      paddingBottom:`50px`,
    }}>
         
         <div className='container mx-auto flex desktop:space-x-6 mobile:block '>
           <div className=' w-1/3 bg-slate-100 rounded-xl shadow-xl p-4 text-center space-y-3 sticky mobile:relative top-0 h-max mobile:w-full'>
               <div className=''><img className='w-28 h-28 mobile:w-10 mobile:h-10 mx-auto rounded-full ' src={avatar==""?'https://t4.ftcdn.net/jpg/02/29/75/83/360_F_229758328_7x8jwCwjtBMmC6rgFzLFhZoEpLobB6L8.jpg':avatar} alt="" /></div>
               <div>
                 <h1 className=' text-xl'>xác minh danh tính</h1>
                 <p className=' font-thin text-xs'>xác thực danh tính của bạn với huy hiệu xác minh danh tính</p>
                 <button> xác thực danh tính </button>
               </div>
               <hr />
               <div className=' space-x-5'>
                   <b>đã xác thực danh tính</b> 
                   <a className=' text-red-500 hover:font-medium' href="#" onClick={()=>{
                      localService.delete("USER_INFOR");
                      window.location.reload();
                   }}>LogOut</a>
               </div>
           </div>
           <div className='w-2/3 bg-slate-200 p-1 rounded-xl mobile:w-full'>
             <h1 className=' text-xl'>Xin chào tôi tên là: <b className=' capitalize'>{name}</b></h1>
               <a  onClick={()=>{setisOpenPopUpEdit((state)=>{return !isOpenPopUpEdit;})}} href="#" className=" underline  hover:text-blue-500">{isOpenPopUpEdit?'lịch sử đặt phòng':"chỉnh sửa hồ sơ"}</a>
              {isOpenPopUpEdit?<>
             <PopUpEditInfor userInfor={userInfor}></PopUpEditInfor>
            </> :
              <div className='mt-5'>
              <h1>Phòng đã thuê</h1>
               <div className='grid grid-cols-1 desktop:grid-cols-2 desktop:gap-2 '>
                 { listBooked.map((item,index)=>{ 
                     return   <ItemPhongDat key={index} data={item}></ItemPhongDat>
                   })
                 }
               </div>
            </div>
             }
           </div>
         </div>
     </div>
  </SercueView>
  )
}

export default UserPage