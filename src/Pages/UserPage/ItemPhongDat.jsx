import moment from 'moment/moment';
import React, { useEffect, useState } from 'react'
import { placesService } from '../../services/API_Airbnb.service';

import { userService } from '../../services/user.service'

function ItemPhongDat({data}) {
    let [room,setRoom]=useState({});
    let {banLa,banUi,bep,dieuHoa,doXe,giaTien,giuong,hinhAnh,hoBoi,khach,maViTri,mayGiat,moTa,phongNgu,phongTam,tenPhong,tivi,wifi}=room;    
    useEffect(()=>{
        placesService.getPhongThue(data.maPhong).then((res)=>{
            setRoom(res.data.content)
        })
    },[])
  return (
    <div className='w-full border-2 rounded-xl border-black p-2 space-y-5'>
            <div className='w-2/3 mx-auto'><img className='w-full' src={hinhAnh} alt="" /></div>
            <div>
            <h1 className=' font-bold'>{tenPhong}:<i>{moment(data.ngayDen).format("DD-MM")+">"+moment(data.ngayDi).format("DD-MM")}</i></h1>
             <p className=' h-20 overflow-y-scroll'>{moTa}</p>
             <p className=' text-right'><b>{giaTien}$</b>/day</p>
            </div>
        </div>
  )
}

export default ItemPhongDat