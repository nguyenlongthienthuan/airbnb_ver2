import moment from 'moment';
import React, { useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import InputSearchCheckIn from '../../components/Search/InputSearchCheckIn';
import InputSearchCheckOut from '../../components/Search/InputSearchCheckOut';
import InputSearchGuest from '../../components/Search/InputSearchGuest';
import { setIsLogin, setIsPopUpBooking } from '../../redux/PopUpHandel';
import { placesService } from '../../services/API_Airbnb.service';
import {localService} from '../../services/local.service'
function DateBooking({roomChoose}) {
    let {id}=useParams();
    let dispatch=useDispatch();
    let [listRooms,setListRooms]=useState([])
    const checkIn=useSelector((state)=>{
        return state.SearchSlice.checkIn;
    })
    const CheckOut=useSelector((state)=>{
        return state.SearchSlice.checkOut;
    })
    const guest=useSelector((state)=>{
        return state.SearchSlice.guest;
    })
     
   useEffect(()=>{
        placesService.getDatPhong().then(
             (res)=>{
           setListRooms(res.data.content);
            }
        )
    },[checkIn , CheckOut])
    let checkRoom=useCallback(()=>{
      let check=true;
     listRooms.forEach((item)=>{
          if(item.maPhong==id){
       if( 
     (moment(checkIn).format('YYYY-MM-DD')<=moment(item.ngayDen).format('YYYY-MM-DD') && moment(CheckOut).format('YYYY-MM-DD')>moment(item.ngayDen).format('YYYY-MM-DD'))
    || (moment(checkIn).format('YYYY-MM-DD')>=moment(item.ngayDen).format('YYYY-MM-DD') && moment(CheckOut).format('YYYY-MM-DD')<=moment(item.ngayDi).format('YYYY-MM-DD')) 
    || (moment(checkIn).format('YYYY-MM-DD')<moment(item.ngayDi).format('YYYY-MM-DD') && moment(CheckOut).format('YYYY-MM-DD')>moment(item.ngayDi).format('YYYY-MM-DD'))   
    ){
       check=false;
     }      
          }
     })
     return check;
  },[checkIn,CheckOut])
    let booking=useCallback(()=>{
      if(checkIn-CheckOut==0 || checkIn-CheckOut>0){document.getElementById("checkOut").innerHTML="chọn ngày trả phòng"}else{document.getElementById("checkOut").innerHTML=""}
      
      if(guest<0 || guest=="" || guest>roomChoose.khach){document.getElementById("slKhach").innerHTML=`0 < khách <= ${roomChoose.khach}`}else{document.getElementById("slKhach").innerHTML=""}
      
       if(localService.get()!=null){
        if (checkIn<CheckOut && guest>0 && guest<=roomChoose.khach && checkRoom()){
            dispatch(setIsPopUpBooking(true))
         }
       }else{
           dispatch(setIsLogin(true))
         alert("hãy đăng nhập để đặt phòng");
       }
     
    
     },[checkIn,CheckOut,guest,roomChoose])
  return (
   <>
    <div className='border-2 shadow-lg w-full h-max p-4 rounded-lg'>
    <div className='flex justify-between p-2'>
     <b>{roomChoose.giaTien}$/đêm</b>
      <span>đánh giá 10 ngôi sao</span>
    </div>
    <hr /> 
    <div className='border-2 rounded-lg my-2'>
      <div className='flex'>
       <div className='w-1/2 border-r-2 border-b-2 p-1'><p className=' text-sm text-gray-400'>nhận phòng</p>
        <InputSearchCheckIn></InputSearchCheckIn>
       <span id='checkIn' className=' text-xs text-red-400'></span>
       </div>
       <div className='w-1/2 border-l-2 border-b-2 p-1'><p className=' text-sm text-gray-400'>trả phòng</p> 
        <InputSearchCheckOut></InputSearchCheckOut>
       <span id='checkOut' className=' text-xs text-red-400'></span>
       </div>
      </div>
      <div className='p-1'><p className=' text-sm text-gray-400'>khách</p>
     <InputSearchGuest></InputSearchGuest>
      <span id='slKhach' className=' text-xs text-red-400'></span>
      </div>  
    </div>
    <div className=' text-center font-bold p-2'>{checkRoom()?<p className=' text-teal-500'>còn phòng</p>:<p className=' text-red-500'>không còn phòng <br /> <i className=' text-xs'>(chọn ngày khác)</i></p>}</div>
    <button onClick={()=>{booking()}} className='w-full bg-red-300 py-3 rounded-lg'>Đặt phòng</button>
   </div>
   </>
  )
}

export default DateBooking