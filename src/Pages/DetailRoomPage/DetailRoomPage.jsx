import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import { useParams } from 'react-router';
import PopUpBooking from '../../components/PopUp/PopUpBooking';
import { placesService } from '../../services/API_Airbnb.service';
import ListComment from './Comment/ListComment';
import DateBooking from './DateBooking';

function DetailRoomPage() {
 
  let viTri=useSelector((state)=>{
      return state.SearchSlice.place.viTri;
  })
  let {id}=useParams();
  let [roomChoose,setRoomChoose]=useState({})
   let {banLa,banUi,bep,dieuHoa,doXe,giaTien,giuong,hinhAnh,hoBoi,khach,maViTri,mayGiat,moTa,phongNgu,phongTam,tenPhong,tivi,wifi}=roomChoose;    
useEffect(()=>{
  window.scrollTo(0,0)
      placesService.getPhongThue(parseInt(id)).then(
          (res)=>{
              setRoomChoose(res.data.content)
          }
      )
   },[id])
return (
 <>
   
 <div style={{
        paddingBottom:`50px`,
      }}>
        <PopUpBooking roomChoose={roomChoose}></PopUpBooking>
   <div className=' p-1 container  mx-auto '>
  
      <hr className=' mt-9 border-2'/>
      <div className='my-2'>
          <h1 className=' font-bold'>{tenPhong}</h1>
          <p className=' text-sm'>{viTri}</p>
      </div>
      <div className='mb-8 mt-8 mobile:mt-0 mobile:mb-0' style={{height:`30vh`}}>
          <img className='w-full h-full rounded-xl object-contain' src={hinhAnh} alt="" />
      </div>
      <div className=' flex p-1 mobile:block capitalize'>
          <div className='w-2/3 p-1 mobile:w-full '>
          <h1>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h1>
          <i>{`${giuong} giường, ${khach} khách, ${phongNgu} phòng ngủ, ${phongTam} phòng tắm`}</i>
          <hr />
          <div id='moTa' className='my-2 '>
              {moTa}
          </div>
          <div id='tienNghi' className='p-1'>
               <h1 className=' font-bold my-2'>Tiện nghi</h1>
               <div className='grid grid-cols-2'>
                      <div><i className="fa fa-utensil-spoon"></i> Bếp: {bep?"yes":"no"}</div>
                      <div>bàn ủi:{banLa?"yes":"no"}</div>
                      <div><i className="fa-solid fa-wind"></i> điều hoà: {dieuHoa?"yes":"no"}</div>
                      <div><i className="fa-solid fa-square-parking"></i> đỗ xe: {doXe?"yes":"no"}</div>
                      <div><i className="fa-solid fa-square-parking"></i> hồ bơi: {hoBoi?"yes":"no"}</div>
                      <div><i className="fa-solid fa-tv"></i> máy giặt: {mayGiat?"yes":"no"}</div>
                      <div><i className="fa-solid fa-tv"></i> tivi: {tivi?"yes":"no"}</div>
                      <div><i className="fa-solid fa-plane-up"></i> wifi: {wifi?"yes":"no"}</div>
               </div>
               <button className='hover:border-4 m-2 rounded-xl border-2 px-2 py-1 capitalize'>hiển thị tất cả 24 tiện nghi</button>
          </div>
          </div>
          <div className='w-1/3 mobile:w-full mobile:mt-5 tablet:w-1/2'>
              <DateBooking roomChoose={roomChoose}></DateBooking>
          </div>
      </div>
      <hr className=' m-3 border-2'/>
      <div className=' mt-3 p-2'>
          <ListComment id={id}></ListComment>
      </div>
  </div>
 </div></>
)
}
export default DetailRoomPage