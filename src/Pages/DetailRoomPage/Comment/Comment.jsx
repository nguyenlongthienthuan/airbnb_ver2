import moment from 'moment';
import React from 'react'

function Comment({ data }) {
  let { avatar, ngayBinhLuan, noiDung, saoBinhLuan, tenNguoiBinhLuan } = data;
  let star = () => {
    let arrayStart = [];
    for (let i = 0; i < saoBinhLuan; i++) {
      arrayStart.push(<i key={i} className="fa fa-star text-xs"></i>)
    }
   
    return arrayStart;

  }
  return (
    <div>
      <div id='top' className=' flex space-x-2 items-center'>
        <div className='w-10 h-10 border-2 border-black rounded-full flex items-center justify-center'><img className=' w-10 h-10 rounded-full object-cover'
          src={avatar == "" ? "https://t4.ftcdn.net/jpg/02/29/75/83/360_F_229758328_7x8jwCwjtBMmC6rgFzLFhZoEpLobB6L8.jpg" : avatar} alt="" /></div>
        <div>
          <h3>{tenNguoiBinhLuan}</h3>
          { star()}
           <i className=' pl-2'>{ngayBinhLuan}</i>
         
        </div>
      </div>
      <div id='bottom'>
        <p className=' pl-3'>{noiDung}</p>
      </div>
    </div>
  )
}

export default Comment