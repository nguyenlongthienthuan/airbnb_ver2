import moment from 'moment/moment';
import React, { useCallback, useEffect, useState } from 'react'
import { placesService } from '../../../services/API_Airbnb.service';
import { localService } from '../../../services/local.service';
import Comment from './Comment';

function ListComment({id}) {
  let [listComment,setListComment]=useState([]);
  let [contentComment,setContentComment]=useState("");
  let [index,setIndex]=useState(3);
    useEffect(()=>{
        placesService.getCommentRoom(id).then(
            (res)=>{
                setListComment(res.data.content);
            }
        )
    },[])
    let renderListComment=()=>{
      return listComment.map((item,indexCmt)=>{  
        if (indexCmt<=index){
          return <Comment key={indexCmt} data={item}></Comment>
        }
      })
    }
    let postComment=useCallback(()=>{
      let params={
        id: 0,
     maPhong: id,
    maNguoiBinhLuan: localService.get().user?.id,
  ngayBinhLuan: moment(new Date()).format("DD/MM/YYYY"),
  noiDung:contentComment,
  saoBinhLuan: 0,
      }
     if(contentComment!=""){
      placesService.postCommentRoom(params).then((res)=>{setContentComment(null)})
     }
    },[id,contentComment])
  return (
  <>
    <div className=' grid grid-cols-2 mobile:grid-cols-1 gap-3'>
        {renderListComment()}
    </div>
    <p onClick={()=>{setIndex((number)=>{return number+3})}} className=' capitalize underline hover:cursor-pointer hover:text-blue-500' href="#">load more</p>
   {localService.get()?   <form className=' space-y-2' >
     <div className=' flex mt-5'>
      <label htmlFor=""><img className='w-10 h-10 rounded-full' src={localService.get().user.avatar==''?"https://t4.ftcdn.net/jpg/02/29/75/83/360_F_229758328_7x8jwCwjtBMmC6rgFzLFhZoEpLobB6L8.jpg":localService.get().user.avatar} alt="" /></label>
      <input onChange={(e)=>{setContentComment(e.target.value)}} className=' w-full rounded-xl p-2 border-2 h-auto' type="text" />
      </div>
     <button onClick={postComment} type='button' className=' p-2 capitalize border-2 rounded-lg'>add comment</button>
      </form>:<></>}
  </>
  )
}

export default ListComment