import moment from 'moment/moment';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useMediaQuery } from 'react-responsive';
import { useNavigate, useParams } from 'react-router';
import { setIsLoading } from '../../redux/PopUpHandel';
import { placesService } from '../../services/API_Airbnb.service';

function ListRoomPage(){
  let dispatch=useDispatch();
  let params=useParams();
  let navigate=useNavigate();
  
  const search=useSelector((state)=>{
      return state.SearchSlice;
  })

  let {place,checkIn,checkOut,guest}=search
  const [listRoom,setListRoom]=useState([])
  let {id}=params;
  useEffect(()=>{
      window.scrollTo(0, 0);
      dispatch(setIsLoading(true));
      placesService.getPhongTheoViTri({maViTri:parseInt(id)}).then(
          (res)=>{
            dispatch(setIsLoading(false));
              let danhSachPhong=res.data.content;
              if(checkIn-checkOut==0){
                 setListRoom(res.data.content.filter((room)=>{
                    if(room.khach<guest){return false}else {return true}
                 }));
              }else{
                  placesService.getDatPhong().then((res)=>{
                      let danhSachPhongDaDat=res.data.content;
                      setListRoom(
                        danhSachPhong.filter((item)=>{
                          let checkRoom=true;
                        
                            if(item.khach<parseInt(guest)){
                                
                                checkRoom=false;
                            }else{
                              
                                danhSachPhongDaDat.forEach((phongDat)=>{
                                    if(item.id==phongDat.maPhong){
                                        if( 
                                            (moment(checkIn).format('YYYY-MM-DD')<moment(phongDat.ngayDen).format('YYYY-MM-DD') && moment(checkOut).format('YYYY-MM-DD')>moment(phongDat.ngayDen).format('YYYY-MM-DD'))
                                           || (moment(checkIn).format('YYYY-MM-DD')>=moment(phongDat.ngayDen).format('YYYY-MM-DD') && moment(checkOut).format('YYYY-MM-DD')<=moment(phongDat.ngayDi).format('YYYY-MM-DD')) 
                                           || (moment(checkIn).format('YYYY-MM-DD')<moment(phongDat.ngayDi).format('YYYY-MM-DD') && moment(checkOut).format('YYYY-MM-DD')>moment(phongDat.ngayDi).format('YYYY-MM-DD'))   
                                           ){
                                            checkRoom=false;
                                        }
                                    }
                                })
                            }
                         return checkRoom;
                      })
                      )
                   
                  })
              }
              
          }
      )
  },[id,checkIn,checkOut,guest]);
return (
  <div >
    
      <div className='' style={{
      paddingBottom:`50px`,
    }}>
          <div className=' m-2 container mx-auto  '>
              <div className='w-full p-2 space-y-2'>
                  <span className=' text-sm text-slate-400'>{place.viTri}</span>
                  <h1 className=' font-bold'>Danh sách các phòng tại {place.viTri} </h1>
                   {checkIn-checkOut==0?<></>:<i>từ {moment(checkIn).format("DD-MM")} tới {moment(checkOut).format("DD-MM")} </i>}
                  <div className='flex [&>*]:border-2 [&>*]:rounded-xl  [&>*]:p-1 space-x-2'>
                    
                  </div>
              </div>
              <hr />
              <div className='p-2 grid grid-cols-2 mobile:grid-cols-1'>
                  {listRoom.length==0?
                         <>
                          <h1>Hiện tại không có danh sách phòng cho địa điểm này!!!</h1>
                         </>:
                     listRoom.map((room,index)=>{
                      return <div key={index} onClick={()=>{navigate(`/detailroom/${room.id}`)}} className=' border-2 rounded-2xl p-2 m-2'>
                          <div className='mb-2 h-28'>
                              <img className='w-full h-full object-contain rounded-xl' src={room.hinhAnh} alt="" />
                          </div>
                          <div className=''>
                              <div><h3 className=' font-bold'>{room.tenPhong}</h3></div>
                              <div className=''><p>{room.moTa?.substring(1,100)+"..."}</p></div>
                              <div className='border-2 rounded-xl w-max p-2 hover:border-4 cursor-pointer'><b>{room.giaTien}$</b>/tháng</div>
                          </div>
                      </div>
                  })}
              </div>
          </div>
          {/* <div className='w-1/2'>googlemap</div> */}
      </div>
    
  </div>
)
}

export default ListRoomPage