import React, { useEffect } from 'react'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router'
import { setPlace } from '../../../redux/SearchSlice'
import { placesService } from '../../../services/API_Airbnb.service'


function PhanTrangTimKiem() {
    const [places, setPlaces] = useState([]);
    const dispatch=useDispatch();
    const navigate=useNavigate();
    useEffect(()=>{
         const params={
            pageIndex:1,
            pageSize:10,
         }
        placesService.getPhanTrangTimKiem({params}).then(
            (res)=>{
                setPlaces(
                  res.data.content.data.map((item)=>{
                    let {tinhThanh,hinhAnh,id}=item
                    return {tinhThanh,hinhAnh,id}})
                    )
            }
        )   
    },[])
  return (
    <div className='p-4 container mx-auto'>
       <div className='w-full overflow-y-scroll'>
       <h1 className=' text-base font-bold m-3 capitalize'>khám phá những điểm đến gần đây</h1>
        <div className=' grid grid-cols-5 mobile:grid-cols-2 p-2 w-full ' style={{height:`20vh`}}>
                {places.map((item,index)=>{
                    return <div  onClick={()=>{navigate(`/listRoom/${item.id}`);dispatch(setPlace( {maViTri:item.id,viTri: `${item.tinhThanh}`}))}} key={index} className=' hover:cursor-pointer flex items-start space-x-2'>
                            <div className='w-10 h-10 hover:w-11'><img className='w-full  rounded-md object-contain' src={item.hinhAnh} alt="" /></div>
                            <div><b className=' text-xs'>{item.tinhThanh}</b></div>
                    </div>
                })}
        </div>
       </div>
    </div>
  )
}

export default PhanTrangTimKiem