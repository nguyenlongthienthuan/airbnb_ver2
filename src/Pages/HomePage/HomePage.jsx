import React from 'react'
import { useMediaQuery } from 'react-responsive'
import Footer from '../../components/Footer/Footer'
import About from './About/About'
import Banner from './Banner/Banner'
import PhanTrangTimKiem from './PhanTrangTimKiem/PhanTrangTimKiem'

function HomePage() {
  return (
    <div style={{
      paddingBottom: `50px`,
    }}>
        <Banner></Banner>
        <PhanTrangTimKiem></PhanTrangTimKiem>
        <About></About>
        <Footer></Footer>
    </div>
  )
}

export default HomePage