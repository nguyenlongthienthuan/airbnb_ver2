import React, { useEffect, useState } from 'react';
import { Layout, Menu, theme } from 'antd';
import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UploadOutlined,
    UserOutlined,
    VideoCameraOutlined,
  } from '@ant-design/icons';

import TableAdmin from './TableAdmin';
import { localService } from '../../services/local.service';
import { useNavigate } from 'react-router';

function AdminPage() {
  let navigate=useNavigate();
    const[page,setPage]=useState(1);
    const [collapsed, setCollapsed] = useState(false);
    const {
      token: { colorBgContainer },
    } = theme.useToken();
    const { Header, Sider, Content } = Layout;
    let handelMenu=(e)=>{
        setPage(parseInt(e.key));
    }
    useEffect(()=>{
      if(localService.get().user.role.toLocaleLowerCase()!="admin"){
          navigate('/');
      }
    })
    return (
        <Layout className='h-screen overflow-scroll'>
          <Sider className='' trigger={null} collapsible collapsed={collapsed}>
            <div className="logo" />
            <Menu
              className=' '
              theme="dark"
              mode="inline"
              defaultSelectedKeys={['1']}
              onClick={(e)=>{handelMenu(e)}}  
              items={[
                {
                  key: '1',
                  icon: <UserOutlined />,
                  label: 'quản lí người dùng ',
                },
                {
                  key: '2',
                  icon: <VideoCameraOutlined />,
                  label: 'quản lí thông tin vị trí',
                },
                {
                  key: '3',
                  icon: <UploadOutlined />,
                  label: 'quản lí thông tin phòng',
                },
                {
                  key: '4',
                  icon: <UploadOutlined />,
                  label: 'quản lí đặt phòng',
                  
                },
              ]}
            />
          </Sider>
          <Layout className="site-layout">
            <Header style={{ padding: 0, background: colorBgContainer }}>
              {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                className: 'trigger',
                onClick: () => setCollapsed(!collapsed),
              })}
            </Header>
            <Content
             className=''
              style={{
                margin: '24px 16px',
                padding: 24,
                minHeight: 280,
                background: colorBgContainer,
                height:`100%`,
                overflow:'scroll',
              }}
            >
               <TableAdmin id={page}></TableAdmin>
              
              
            </Content>
             
          </Layout>
        </Layout>
      );
}

export default AdminPage