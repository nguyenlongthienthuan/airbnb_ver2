
import React, { useCallback, useEffect, useState } from 'react';
import { Button, Form, Input, Space, Table, Tag } from 'antd';
import { API_ADMIN_SERVICE } from '../../services/API_AdminAirbnb.service';
import { placesService } from '../../services/API_Airbnb.service';
import { localService } from '../../services/local.service';


function TableAdmin({id}) {
  let [data,setData]=useState([]);
  let [columns,setColumns]=useState([]);

  let [searchKey,setSearchKey]=useState("");
  let [pageIndex,setPageIndex]=useState(1);
  let [checkKey,setCheckKey]=useState({});
  let [showMore,setShowMore]=useState({});
  const [form]=Form.useForm();
  const onFinish=(values)=>{
   
   if (parseInt(id)==1){
    API_ADMIN_SERVICE.putUserId(values.id,{...values}).then((res)=>{
      window.location.reload();
    })
  }
  else if (parseInt(id)==2){
    API_ADMIN_SERVICE.putViTri(values.id,{...values}).then((res)=>{
      window.location.reload();
    })
  }
  else if (parseInt(id)==3){
  API_ADMIN_SERVICE.putPhong(values.id,{...values}).then((res)=>{
    window.location.reload();
  })
  }
  else if (parseInt(id)==4){
    API_ADMIN_SERVICE.putDatPhong(values.id,{...values}).then((res)=>{
      window.location.reload();
    })
  }

}
  useEffect(()=>{
   if(data.length>0){
  
   let cloneObj=Object.getOwnPropertyNames(data[0]);
      cloneObj.push('action');
    setColumns(  cloneObj.map((item)=>{
      return {
        title:item,
        dataIndex:item,
        key:item,
        render: (_,text) => {
          
          if(item!='avatar' && item!='password'){
            if (text.stt==checkKey.key ){
              if(item!='action' && item!='stt'){
                let regex=()=>{
                 if(item=='name'){
                    return [{
                    // required: true,
                    // type: "regexp",
                    pattern: new RegExp(/^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]*$/),
                    message: "tên mà có số hả ba!"
                },{
                  required: true,
                  message: "Không dc để trống!"
                 }];
                 }
                 else if(item=='email'){ 
                  return [{
                    pattern: new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/),
                    message: "không đúng định dạng email"
                },{
                  required: true,
                  message: "Không dc để trống!"
                 }];
                }
                 else if(item=='phone'){ 
                  return [{
                    pattern: new RegExp(/^[0-9\-\+]{9,10}$/),
                    message: "Nhập sdt 10 số"
                },{
                  required: true,
                  message: "Không dc để trống!"
                 }];
                }
                else{
                  return {
                    required: true,
                    message: "Wrong format!"
                }
                }
                
                }
                return <Form.Item  rules={
                 Array.isArray(regex())?[...regex()]:[{...regex()}]
                 } name={item}>
                  <Input className=' w-max'
                    
                  />
                </Form.Item>
              }else{
                if(item=='action'){
                  return <button type='submit' className='w-full h-full flex justify-start items-start capitalize hover:text-green-500'>update</button>
                }
               }
             } 
           }
        
     if (item!='action' ){
      if(text[item]==true && text[item]!=1){return <p className='w-max'>yes</p> }else if(text[item]==false && text[item]!=0){return <p className='w-max'>no</p>}
      if (!isNaN(text[item])){
        return text[item]
       }else {
        if(text[item]!=undefined){
          if(text[item].length<100){
            return <p className='w-max'>{text[item]}</p>
        }else{
          return <p className=' hover:cursor-pointer w-max' onClick={()=>{setShowMore({obj:item,key:text.stt});}}>{showMore.obj===item && showMore.key===text.stt?text[item]:"..."}</p>;
        }
        }
       }
     }else{
       return <div className='flex space-x-1 '>
       <button onClick={()=>{
        setCheckKey({obj:item,key:text.stt})
       let index=data.findIndex((item)=>{return item.id==text.id});
        form.setFieldsValue(data[index]);
        }} className='capitalize hover:text-blue-500'>edit</button>
       <button onClick={()=>{
         if(parseInt(id)==1){
          API_ADMIN_SERVICE.deleteUser({id:parseInt(text.id)}).then((res)=>{
             window.location.reload();
           })
           }
         else if (parseInt(id)==2){
          API_ADMIN_SERVICE.deleteViTri(parseInt(text.id)).then((res)=>{
            window.location.reload();
          })
          }
          else if (parseInt(id)==3){
            API_ADMIN_SERVICE.deletePhong(text.id).then((res)=>{
              window.location.reload();
            })
           }
        else if (parseInt(id)==4){
          API_ADMIN_SERVICE.deleteDatPhong(text.id).then((res)=>{
            window.location.reload();
          })
          }
       }} className='capitalize hover:text-red-500'>delete</button>
       </div>
     }
           
        }
    }
    }))
   }
   
  },[checkKey,data,showMore])
    useEffect(()=>{
      if (parseInt(id)==1){
        API_ADMIN_SERVICE.getListUser({pageIndex,pageSize:99999999,keyword:searchKey}).then((res)=>{
          setData( res.data.content.data.map((item,index)=>{
            let clone=Object.assign({stt: index+1}, item);
             return clone;
             
         }));
       
        })
      }
      else if (parseInt(id)==2){
        let params={
          pageIndex,
          pageSize:9999999,
        }
       placesService.getPhanTrangTimKiem({params}).then((res)=>{
         setData( 
          res.data.content.data.map((item,index)=>{
           let clone=Object.assign({stt: index+1}, item);
             return clone;
             
         })
         );
        })
      }
      else if (parseInt(id)==3){
        let params={
          pageIndex,
          pageSize:9999999,
        }
       placesService.getPhongPhanTrangTimKiem(params).then((res)=>{
         setData( res.data.content.data.map((item,index)=>{
           let clone=Object.assign({stt: index+1}, item);
             return clone;
         }));
        })
      }
      else if (parseInt(id)==4){
       
       placesService.getDatPhong().then((res)=>{
        setData( res.data.content.map((item,index)=>{
          let clone=Object.assign({stt: index+1}, item);
           return clone;
           
       }));
        })
      }
     setCheckKey({})
    },[id,searchKey])
    let defaultPageSize=9;
   
  return (
  <>
  <Form onFinish={onFinish} form={form}  action="">
  <input onChange={(e)=>{setSearchKey(e.target.value)}} placeholder='search email or name' className='border-2 mb-1' type="text" />
  <Table   id='table-tht'
   className=' w-full hover:overflow-scroll '
   pagination={{ defaultPageSize, showSizeChanger: true, pageSizeOptions: ['10', '20', '30']}}  columns={columns} dataSource={data} >
</Table>
  </Form>
   
</>
  )
}

export default TableAdmin
 