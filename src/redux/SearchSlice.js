import { createSlice } from "@reduxjs/toolkit"

const initialState={
    place:{maViTri:"",viTri:''},
    checkIn:new Date(),
    checkOut:new Date(),
    guest:"",
}
const SearchSlice=createSlice({
    name:"SearchSlice",
    initialState,
    reducers:{
        setPlace:(state,{payload})=>{
            state.place=payload;
        },
        setCheckIn:(state,{payload})=>{
            state.checkIn=payload;
        },
        setCheckOut:(state,{payload})=>{
            state.checkOut=payload;
        },
        setGuest:(state,{payload})=>{
            state.guest=payload;
        },
    }
})
export const{setPlace,setCheckOut,setCheckIn,setGuest}=SearchSlice.actions;
export default SearchSlice.reducer;