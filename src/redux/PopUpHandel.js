import { createSlice } from "@reduxjs/toolkit"

const initialState={
    isPopUp:false,
    isLogin:false,
    isPopUpBooking:false,
    isLoading:false,
}
const PopUpHandelSlice=createSlice({
    name:"PopUpHandelSlice",
    initialState,
    reducers:{
        setIsLogin:(state,{payload})=>{
            state.isLogin=payload;
        },
        setIsPopUp:(state,{payload})=>{
            state.isPopUp=payload;
        },
        setIsPopUpBooking:(state,{payload})=>{
            state.isPopUpBooking=payload;
        },
        setIsLoading:(state,{payload})=>{
            state.isLoading=payload;
        }
    }
})
export const {setIsLogin,setIsPopUp,setIsPopUpBooking,setIsLoading}=PopUpHandelSlice.actions;
export default PopUpHandelSlice.reducer;