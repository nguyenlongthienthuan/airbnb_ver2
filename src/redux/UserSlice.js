import { localService } from "../services/local.service";

const { createSlice } = require("@reduxjs/toolkit");

let initialState={
    userInfo:localService.get(),
  
}
 let UserSlice=createSlice({
    name:"UserSlice",
    initialState,
    reducers:{
        setUserInfor:(state,{payload})=>{
            state.userInfo=payload;
        },
       
    }
 })
 export let {setUserInfor}=UserSlice.actions;
 export default UserSlice.reducer;