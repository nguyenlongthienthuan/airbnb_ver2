import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import './tailwind.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { configureStore } from '@reduxjs/toolkit';
import { RouterProvider } from 'react-router';
import { Provider } from 'react-redux';
import SearchSlice from './redux/SearchSlice';
import PopUpHandel from './redux/PopUpHandel';
import { getDefaultMiddleware } from '@reduxjs/toolkit';
import UserSlice from './redux/UserSlice';
const customizedMiddleware = getDefaultMiddleware({
  serializableCheck: false
})
export const store=configureStore({
  reducer:{
    SearchSlice,
    PopUpHandel,
    UserSlice,
  },
  middleware:customizedMiddleware,
})
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
<Provider store={store}>
<App />
</Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
