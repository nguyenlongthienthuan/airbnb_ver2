import React, { useEffect } from 'react'
import { useNavigate } from 'react-router';
import { localService } from '../services/local.service';

function SercueView({children}) {
    let navigate=useNavigate()
    useEffect(()=>{
        let user=localService.get();
        if(user==null){
            navigate("/")
        }
    })
  return children
}

export default SercueView