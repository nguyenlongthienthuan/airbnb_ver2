
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './Pages/HomePage/HomePage';
import ListRoomPage from './Pages/ListRoomPage/ListRoomPage';

import SignUpPage from './Pages/SignUpPage/SignUpPage';
import DetailRoomPage from './Pages/DetailRoomPage/DetailRoomPage';
import PopUpLogin from './components/PopUp/PopUpLogin';
import "react-datepicker/dist/react-datepicker.css";
import Header from './components/Header/Header';
import UserPage from './Pages/UserPage/UserPage';
import AdminPage from './Pages/Admin/AdminPage';
import Loading from './components/Loading/Loading';
function App() {
  return (
    <div className="">
      <Loading></Loading>
      
     <BrowserRouter>
     <Header></Header>
     <PopUpLogin></PopUpLogin>
     <Routes>
      <Route path='/' element={<HomePage></HomePage>}>
      </Route>
      <Route path='/listRoom/:id' element={<ListRoomPage></ListRoomPage>}>
      </Route>
      <Route path='/signup' element={<SignUpPage></SignUpPage>}>
      </Route>
      <Route path='/detailroom/:id' element={<DetailRoomPage></DetailRoomPage>}>
      </Route>
      <Route path='/user/:id' element={<UserPage></UserPage>}>
      </Route>
      <Route path='/admin' element={<AdminPage></AdminPage>}>
      </Route>
      
     </Routes>
     
     </BrowserRouter>
    </div>
  );
}

export default App;
