/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      screens: {

        'mobile': {'max':'767px'},
        'tablet': {'max':'991px','min':'768px'},
        // => @media (min-width: 640px) { ... }
  
        // 'laptop': '1024px',
        // => @media (min-width: 1024px) { ... }
  
        'desktop': '992px',
        // sm: "600px",
        // md: "728px",
        // lg: "984px",
        // xl: "1240px",
        // "2xl": "1320px",
      },
      height: {
        '30vh': '30vh',
        '60vh': '60vh',
      },
      spacing: {
        '110%':'110%',
        '-150%': '-150%',
      }

    },
  },
  plugins: [],
}
